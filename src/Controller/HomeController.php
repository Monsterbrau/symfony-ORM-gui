<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\Golem;

class HomeController extends Controller
{
    /**
     * @Route("/", name="home")
     */
    public function index()
    {
        // $soldierGolem = new Golem;
        // $soldierGolem->setName("Nori");
        // $soldierGolem->setMaterial("iron");
        // $soldierGolem->setDateConception(new \DateTime("2018-06-01"));

        // $em = $this->getDoctrine()->getManager();
        // $em->persist($soldierGolem);
        // $em->flush();
        // $repo = $this->getDoctrine()->getRepository(Golem::class);
        // dump($repo->findAll());
        // dump($repo->find(1));
        // dump($repo->findBy(['name'=>'orion']));

        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }
}
