<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\GolemRepository;
use App\Entity\Golem;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Doctrine\DBAL\Types\DateType;
use App\Form\GolemType;

class AddGolemController extends Controller
{
    /**
     * @Route("/add/golem", name="add_golem")
     */
    public function index(Request $request)
    {
        $form= $this->createForm(GolemType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($form->getData());
            $em->flush(); 
        }
        

        return $this->render('add_golem/index.html.twig', [
            'form'=> $form->createView(),
        ]);
    }
}
