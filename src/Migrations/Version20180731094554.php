<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180731094554 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE guild (id INT AUTO_INCREMENT NOT NULL, energy TINYINT(1) NOT NULL, location VARCHAR(255) NOT NULL, superior INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE guild_golem (guild_id INT NOT NULL, golem_id INT NOT NULL, INDEX IDX_D5BED01F5F2131EF (guild_id), INDEX IDX_D5BED01FBC5A4E27 (golem_id), PRIMARY KEY(guild_id, golem_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE guild_golem ADD CONSTRAINT FK_D5BED01F5F2131EF FOREIGN KEY (guild_id) REFERENCES guild (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE guild_golem ADD CONSTRAINT FK_D5BED01FBC5A4E27 FOREIGN KEY (golem_id) REFERENCES golem (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE guild_golem DROP FOREIGN KEY FK_D5BED01F5F2131EF');
        $this->addSql('DROP TABLE guild');
        $this->addSql('DROP TABLE guild_golem');
    }
}
