<?php

namespace App\Repository;

use App\Entity\Golem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Golem|null find($id, $lockMode = null, $lockVersion = null)
 * @method Golem|null findOneBy(array $criteria, array $orderBy = null)
 * @method Golem[]    findAll()
 * @method Golem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GolemRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Golem::class);
    }

//    /**
//     * @return Golem[] Returns an array of Golem objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Golem
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
