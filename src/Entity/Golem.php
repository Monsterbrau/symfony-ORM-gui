<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GolemRepository")
 */
class Golem
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $material;

    /**
     * @ORM\Column(type="date")
     */
    private $dateConception;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Guild", mappedBy="property")
     */
    private $guilds;

    public function __construct()
    {
        $this->guilds = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getMaterial(): ?string
    {
        return $this->material;
    }

    public function setMaterial(string $material): self
    {
        $this->material = $material;

        return $this;
    }

    public function getDateConception(): ?\DateTimeInterface
    {
        return $this->dateConception;
    }

    public function setDateConception(\DateTimeInterface $dateConception): self
    {
        $this->dateConception = $dateConception;

        return $this;
    }

    /**
     * @return Collection|Guild[]
     */
    public function getGuilds(): Collection
    {
        return $this->guilds;
    }

    public function addGuild(Guild $guild): self
    {
        if (!$this->guilds->contains($guild)) {
            $this->guilds[] = $guild;
            $guild->addProperty($this);
        }

        return $this;
    }

    public function removeGuild(Guild $guild): self
    {
        if ($this->guilds->contains($guild)) {
            $this->guilds->removeElement($guild);
            $guild->removeProperty($this);
        }

        return $this;
    }
}
