<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GuildRepository")
 */
class Guild
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $energy;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $location;

    /**
     * @ORM\Column(type="integer")
     */
    private $superior;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Golem", inversedBy="guilds")
     */
    private $property;

    public function __construct()
    {
        $this->property = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getEnergy(): ?bool
    {
        return $this->energy;
    }

    public function setEnergy(bool $energy): self
    {
        $this->energy = $energy;

        return $this;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(string $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getSuperior(): ?int
    {
        return $this->superior;
    }

    public function setSuperior(int $superior): self
    {
        $this->superior = $superior;

        return $this;
    }

    /**
     * @return Collection|Golem[]
     */
    public function getProperty(): Collection
    {
        return $this->property;
    }

    public function addProperty(Golem $property): self
    {
        if (!$this->property->contains($property)) {
            $this->property[] = $property;
        }

        return $this;
    }

    public function removeProperty(Golem $property): self
    {
        if ($this->property->contains($property)) {
            $this->property->removeElement($property);
        }

        return $this;
    }
}
